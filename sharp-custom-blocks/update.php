<?php

/* $res empty at this step
 * $action 'plugin_information'
 * $args stdClass Object ( [slug] => woocommerce [is_ssl] => [fields] => Array ( [banners] => 1 [reviews] => 1 [downloaded] => [active_installs] => 1 ) [per_page] => 24 [locale] => en_US )
*****************************************************************************************************/
add_filter('plugins_api', function( $res, $action, $args ) use($plugin_info,$plugin_slug,$plugin_transient){
 
	// do nothing if this is not about getting plugin information
	if( 'plugin_information' !== $action ) {
		return false;
	}
 
	// do nothing if it is not our plugin
	if( $plugin_slug !== $args->slug ) {
		return false;
	}
 
	// trying to get from cache first
	if( false == $remote = get_transient( $plugin_transient ) ) {
 
		// info.json is the file with the actual plugin information on your server
		$remote = wp_remote_get( $plugin_info . '/info.json', array( 'timeout' => 10, 'headers' => array( 'Accept' => 'application/json' ) ) );
 
		if ( ! is_wp_error( $remote ) && isset( $remote['response']['code'] ) && $remote['response']['code'] == 200 && ! empty( $remote['body'] ) ) {
			set_transient( $plugin_transient, $remote, (60*60*12) ); // 12 hour cache
		}
 
	}
 
	if( ! is_wp_error( $remote ) && isset( $remote['response']['code'] ) && $remote['response']['code'] == 200 && ! empty( $remote['body'] ) ) {
 		
 		$remote = json_decode( $remote['body'] );
		$res = new stdClass();

		$res->name = $remote->name;
		$res->slug = $plugin_slug;
		$res->version = $remote->version;
		$res->tested = $remote->tested;
		$res->requires = $remote->requires;
		$res->author = '<a href="https://www.sharpinnovations.com">Sharp Innovations</a>';
		$res->author_profile = 'https://www.sharpinnovations.com';
		$res->download_link = $remote->download_url;
		$res->trunk = $remote->download_url;
		$res->requires_php = $remote->requires_php;
		$res->last_updated = $remote->last_updated;
		$res->sections = array(
			'description' => $remote->sections->description,
			'installation' => $remote->sections->installation,
			'changelog' => $remote->sections->changelog
			// you can add your custom sections (tabs) here
		);
 
		// in case you want the screenshots tab, use the following HTML format for its content:
		// <ol><li><a href="IMG_URL" target="_blank"><img src="IMG_URL" alt="CAPTION" /></a><p>CAPTION</p></li></ol>
		if( !empty( $remote->screenshots ) ) {
			$res->sections['screenshots'] = $remote->screenshots;
		}
 
		$res->banners = array(
			'low' => $remote->banners->low,
			'high' => $remote->banners->high,
		);
		return $res;
 
	}
 
	return false;
 
}, 20, 3);

/* Push the Update Information into WP Transients
*****************************************************************************************************/
add_filter('site_transient_update_plugins', function( $transient ) use($plugin_version,$plugin_info,$plugin_slug,$plugin_transient){
 
	if ( empty($transient->checked ) ) {
        return $transient;
    }
 
	// trying to get from cache first, to disable cache comment 10,20,21,22,24
	if( false == $remote = get_transient( $plugin_transient ) ) {
 
		// info.json is the file with the actual plugin information on your server
		$remote = wp_remote_get( $plugin_info . '/info.json', array( 'timeout' => 10, 'headers' => array( 'Accept' => 'application/json' ) ) ); 
 
		if ( !is_wp_error( $remote ) && isset( $remote['response']['code'] ) && $remote['response']['code'] == 200 && !empty( $remote['body'] ) ) {
			set_transient( $plugin_transient, $remote, (60*60*12) ); // 12 hour cache
		}
 
	}

	if( $remote ) {
 	
		$remote = json_decode( $remote['body'] );
 
		// your installed plugin version should be on the line below! You can obtain it dynamically of course 
		if( $remote && version_compare( $plugin_version, $remote->version, '<' ) && version_compare($remote->requires, get_bloginfo('version'), '<' ) ) {
			$res = new stdClass();
			$res->slug = $plugin_slug;
			$res->plugin = $plugin_slug . '/' . $plugin_slug . '.php'; // it could be just YOUR_PLUGIN_SLUG.php if your plugin doesn't have its own directory
			$res->new_version = $remote->version;
			$res->tested = $remote->tested;
			$res->package = $remote->download_url;
       		
       		$transient->response[$res->plugin] = $res;
       		$transient->checked[$res->plugin] = $remote->version;
        }
 
	}
       
    return $transient;
} );

/* Add view details link as a standard item
*****************************************************************************************************/
add_filter('plugin_row_meta', function( $plugin_meta, $plugin_file, $plugin_data, $status ) use($plugin_slug){

	// do nothing if it is not our plugin
	if( $plugin_slug !== $plugin_data['TextDomain'] ) {
		return $plugin_meta;
	}

	$plugin_meta[] = sprintf(
		'<a href="%s" class="thickbox open-plugin-details-modal" aria-label="%s" data-title="%s">%s</a>',
		esc_url(
			network_admin_url(
				'plugin-install.php?tab=plugin-information&plugin=' . $plugin_slug .
				'&TB_iframe=true&width=600&height=550'
			)
		),
		esc_attr( sprintf( __( 'More information about %s' ), $plugin_slug ) ),
		esc_attr( $plugin_slug ),
		__( 'View details' )
	);
	
	return $plugin_meta;
}, 10, 4 );

/* Cache the results to make it awesomely fast
*****************************************************************************************************/
add_action('upgrader_process_complete', function( $upgrader_object, $options ) use($plugin_transient){
	if ( $options['action'] == 'update' && $options['type'] === 'plugin' )  {
		// just clean the cache when new plugin version is installed
		delete_transient( $plugin_transient );
	}
}, 10, 2 );
