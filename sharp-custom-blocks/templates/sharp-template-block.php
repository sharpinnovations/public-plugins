<?php

// CONFIG
$blockID = str_replace( '_', '-', ( !empty($block['id']) ? $block['id'] : '' ) );
$blockClass = str_replace( 'acf/','block-', ( !empty($block['name']) ? $block['name'] : '' ) );
$blockClass .= !empty($block['className']) ? ' ' . $block['className'] : '' ;
$blockClass .= !empty($block['align']) ? ' align' . $block['align'] : '' ;

// OPTIONS
$textExample = get_field('textExample');
$textAreaExample = get_field('textAreaExample');
$wysiwygExample = get_field('wysiwygExample');
$selectExample = get_field('selectExample');
$imageExample = get_field('imageExample');
$galleryExample = get_field('galleryExample');

// START LAYOUT WRAPPER
echo '<div'
	. ' id="' . $blockID . '"'
	. ' class="' . $blockClass . '"'
	. ' ' . (is_admin()?'onclick="event.preventDefault();"':'')
. '>';
	
	// CUSTOM TEMPLATE
	echo '<br /><strong>TEXT</strong><br />';
	print_r($textExample);
	
	echo '<br /><strong>TEXTAREA</strong><br />';
	print_r($textAreaExample);
	
	echo '<br /><strong>WYSIWYG</strong><br />';
	print_r($wysiwygExample);
	
	echo '<br /><strong>SELECT</strong><br />';
	print_r($selectExample);
	
	echo '<br /><strong>IMAGE</strong><br />';
	print_r($imageExample);
	
	echo '<br /><strong>GALLERY</strong><br />';
	print_r($galleryExample);

// END LAYOUT
echo '</div>';
