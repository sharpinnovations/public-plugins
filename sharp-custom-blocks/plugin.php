<?php
/**
 * @package SharpCustomBlocks
 * @author PStevenson
 * @author Sharp Innovations
 * @copyright Copyright (c) 2024
 * @license None - All Rights Reserved
 * @version 1.0.0
 **/
namespace SharpCustomBlocks;
class Setup { 

	/**
	 * Initializes Theme Specific Items
	 **/
	static public function _init(){
		
		add_action( 'block_categories', [__CLASS__,'createBlockCategories'], 10, 2 );

		// AUTOLOAD BLOCKS
		foreach(glob(__DIR__.'/blocks/*.php') as $file):

			require_once($file);

			$blockName = substr(basename($file),0,-4);
			if(is_callable(__NAMESPACE__.'\\'.$blockName.'::customFields')):
				add_action( 'acf/init', [__NAMESPACE__.'\\'.$blockName,'customFields'] );
			endif;

		endforeach;

	}

	/**
	 * Sets block to Template
	 **/
	static public function createBlockCategories( $categories, $post ) {	
		return array_merge(
			[[
				'slug'	=> 'sharpinnovations',
				'title'	=> __('Theme Blocks','sharpinnovations'),
				'icon'	=> 'yes',
			]],
			$categories
		);
	}

	/**
	 * Sets block to Template
	 **/
	static public function createBlockView( $block ) {

		$SLUG = strtolower( str_replace( 'acf/', '', !empty($block['name']) ? $block['name'] : 'missing_block' ) );
	
		$PATH = get_theme_file_path('/sharp-custom-blocks/'.$SLUG.'.php'); // let's you override this in your theme
		$PATH = file_exists($PATH)? $PATH : __DIR__.'/templates/'.$SLUG.'.php'; // uses this as a fallback

		if(file_exists($PATH)):

			ob_start();
			include($PATH);
			$blockContent = ob_get_clean();

			if(is_admin()):
				echo preg_replace('/<script([^>]*)?>(.)*?<\/script>/i','',$blockContent);
			else:
				echo $blockContent;
			endif;
			
		else:
			if(is_admin()):
				echo '<h3 class="wpps-acf-block-title" style="margin:0 -15px 0;padding:5px 15px;background:#EEE;line-height:1.4em;color:#BBB;">WARNING:</h3>';
				echo '<p class="wpps-acf-block-description" style="margin:0 -15px 20px;padding:5px 15px;background:#EEE;line-height:1.4em;color:#BBB;"><em>Block template not available!</em></p>';
			endif;
		endif;

	}

}
