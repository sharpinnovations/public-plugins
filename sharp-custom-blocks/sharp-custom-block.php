<?php
/*!
 * @wordpress-plugin
 * Plugin Name:		Sharp | Custom Blocks
 * Plugin URI:		https://bitbucket.org/sharpinnovations/public-plugins/src/master/sharp-custom-blocks/
 * Description:		This plugin enabled share custom blocks created by Sharp Innovations.
 * Version:			1.0.0
 * Author:			Sharp Innovations
 * Author URI:		https://www.sharpinnovations.com
 **/

// https://developer.atlassian.com/bitbucket/api/2/reference/resource/
// https://rudrastyh.com/wordpress/self-hosted-plugin-update.html

/* Settings
*****************************************************************************************************/
$plugin_info = 'https://api.bitbucket.org/2.0/repositories/sharpinnovations/public-plugins/src/master/sharp-custom-blocks';

$plugin_version = '1.0.0';
$plugin_slug = 'sharp-custom-blocks';

$plugin_transient = 'sharp-update'
	. '--sharp-custom-blocks'
	. '--' . $plugin_slug;

// error_reporting(E_ALL);
// ini_set('display_errors',1);

/* LOAD PLUGIN CODE
*****************************************************************************************************/
require_once(__DIR__.'/update.php');

if(
	!file_exists(WP_PLUGIN_DIR.'/advanced-custom-fields-pro/acf.php')
	|| !is_plugin_active('advanced-custom-fields-pro/acf.php') 
):
	return;
else:
	require_once(__DIR__.'/plugin.php');
	\SharpCustomBlocks\Setup::_init();
endif;

add_action('admin_init',function(){
	if(
		!file_exists(WP_PLUGIN_DIR.'/advanced-custom-fields-pro/acf.php')
		|| !is_plugin_active('advanced-custom-fields-pro/acf.php') 
	):
		add_action('admin_notices',function(){
			echo '<div class="error notice"><p>' . __( 'Advanced Custom Fields (ACF) PRO plugin is required for the best with this theme. <a href="https://www.advancedcustomfields.com/" target="_blank">Download & install now!</a>') . ' </p></div>';
		},100);
	endif;
});
