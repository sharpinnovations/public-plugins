<?php
/**
 * @package SharpCustomBlocks
 * @author PStevenson
 * @copyright Copyright (c) 2024
 * @license None - All Rights Reserved
 * @version 1.0.0
 **/
namespace SharpCustomBlocks;
class TemplateBlock { 

	/**
	 * Sets up the custom fields
	 **/
	static public function customFields(){
		
		$BLOCK_NMSPC = strtolower('sharp-template-block');
		$BLOCK_TITLE = __('Template Block');
		$BLOCK_PREFX = 'sharp_';
		
		if(function_exists('acf_register_block')):		
			acf_register_block([
				'name'				=> $BLOCK_NMSPC,
				'title'				=> $BLOCK_TITLE,
				'description'		=> $BLOCK_TITLE . __(' Custom Template Block.'),
				'render_callback'	=> [__NAMESPACE__.'\\Setup','createBlockView'],
				'category'			=> 'postscript',
				'icon'				=> 'editor-kitchensink', // https://developer.wordpress.org/resource/dashicons/#format-image
				'keywords'			=> [$BLOCK_NMSPC,$BLOCK_TITLE,'sharp','sharpinnovations'],
			]);
		endif;

		if(function_exists('acf_add_local_field_group')):
			acf_add_local_field_group([
				'key'		=> $BLOCK_NMSPC,
				'title'		=> $BLOCK_TITLE,
				'fields'	=> [
					[
						'required'		=> false,
						'key' 			=> $BLOCK_PREFX.'textExample',
						'name' 			=> 'textExample',
						'label' 		=> 'TextExample',
						'instructions' 	=> '',
						'type' 			=> 'text',
						'default_value' => '',
						'placeholder' 	=> '',
						'formatting' 	=> 'html',
						'wrapper' 		=> [ 'width' => 100, ],
					],
					[
						'required'		=> false,
						'key' 			=> $BLOCK_PREFX.'textAreaExample',
						'name' 			=> 'textAreaExample',
						'label' 		=> 'Text Area Example',
						'instructions' 	=> '',
						'type' 			=> 'textarea',
						'default_value' => '',
						'placeholder' 	=> '',
						'formatting' 	=> 'html',
						'wrapper' 		=> [ 'width' => 100, ],
					],
					[
						'required'		=> false,
						'key' 			=> $BLOCK_PREFX.'wysiwygExample',
						'name' 			=> 'wysiwygExample',
						'label' 		=> 'Wysiwyg Example',
						'instructions' 	=> '',
						'type' 			=> 'wysiwyg',
						'default_value' => '', 'toolbar' => 'full', 'media_upload' => 'yes',
						'wrapper' 		=> [ 'width' => 100, ],
					],
					[
						'required'		=> false,
						'key' 			=> $BLOCK_PREFX.'selectExample',
						'name' 			=> 'selectExample',
						'label' 		=> 'Select Example',
						'instructions' 	=> '',
						'type' 			=> 'select', // select / radio / checkbox
						'default_value' => '',
						'placeholder' 	=> '',
						'formatting' 	=> 'html',
						'wrapper' 		=> [ 'width' => 100, ],
						'multiple' 		=> false,
						'choices' 		=> [
							'yes' 			=> 'Yes',
							'no' 			=> 'No',
						],
					],
					[
						'required'		=> false,
						'key' 			=> $BLOCK_PREFX.'imageExample',
						'name' 			=> 'imageExample',
						'label' 		=> 'Image Example',
						'instructions' 	=> '',
						'type' 			=> 'image',
						'save_format' 	=> 'id', 
						'preview_size' 	=> 'thumbnail', 
						'library' 		=> 'all',
						'wrapper' 		=> [ 'width' => 100, ],
					],
					[
						'required'		=> false,
						'key' 			=> $BLOCK_PREFX.'galleryExample',
						'name' 			=> 'galleryExample',
						'label' 		=> 'Gallery Example',
						'instructions' 	=> '',
						'type' 			=> 'gallery',
						'save_format' 	=> 'id', 
						'preview_size' 	=> 'thumbnail', 
						'library' 		=> 'all',
						'wrapper' 		=> [ 'width' => 100, ],
					],
				],
				'location' => [[[
					'param'=>'block',
					'operator'=>'==',
					'value'=>'acf/'.$BLOCK_NMSPC,
				]]],
				'position' 			=> 'normal',
				'style' 			=> 'default',
				'hide_on_screen' 	=> [],
				'active' 			=> 1,
				'description' 		=> '',
				'label_placement' 	=> 'top',
				'instruction_placement' => 'label',
			]);
		endif;

	}

}