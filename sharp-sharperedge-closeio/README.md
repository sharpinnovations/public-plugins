### Upload the plugin to your site and activate it. From there, here are items need to add to your site config:

--

** define('SHARPEREDGE_CLOSEIO_APIKEY','INSERT_KEY_HERE'); **

* REQUIRED: API Key used in CloseIO to allow data insertion.

** define('SHARPEREDGE_CLOSEIO_GRAVITY_ID',[1,2,3]); **

* OPTIONAL: Form ID's that should sync data. If not defined, it will default to ALL forms.

** define('SHARPEREDGE_CLOSEIO_STATUS','Potential'); **

* OPTIONAL: Default status to be used in CloseIO.

**
const SHARPEREDGE_CLOSEIO_CUSTOMFIELDS = [
	'.' => [
		'custom.INSERT_ID_HERE' => 'value',
	],
	'1' => [
		'custom.INSERT_ID_HERE' => 'value',
	],
];
**

* OPTIONAL: Add optional custom fields to be set on leads. '.' is used on all forms, '#' is for specific form IDs.
