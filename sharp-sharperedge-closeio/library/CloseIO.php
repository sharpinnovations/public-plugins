<?php
namespace SharperEdge\API;
class CloseIO {

	/**
	 * API Username
	 * @return string
	 **/
	static private function apiUsername() {
		return defined('SHARPEREDGE_CLOSEIO_APIKEY')? SHARPEREDGE_CLOSEIO_APIKEY : '';
	}

	/**
	 * API Password
	 * @return string
	 **/
	static private function apiPassword() {
		return '';
	}

	/**
	 * API KEY
	 * @return string
	 **/
	static private function apiKey() {
		return base64_encode(self::apiUsername().':'.self::apiPassword());
	}

	/**
	 * API RESPONSE
	 * @param array $data
	 * @param string $useCache
	 * @return array
	 **/
	static public function api( $data = [], $useCache = '+30 seconds' ) {

		$dataDefault = [
			'fields'		=> [],
			'endpoint'		=> '/lead/',
			'request'		=> 'get',
		];
		$data = (object) array_merge($dataDefault,$data);
		$cacheKey = 'closeapi_'.self::apiKey().'_cache_'.json_encode($data);
		$cacheTimeKey = 'closeapi_'.self::apiKey().'_cache_time_'.json_encode($data);

		if(
			$useCache
			&& !empty($_SESSION[$cacheKey])
			&& !empty($_SESSION[$cacheTimeKey])
			&& time() < $_SESSION[$cacheTimeKey]
		):
			
			$response = $_SESSION[$cacheKey];

		else:

			for( $tryApi = 0; $tryApi < 3; $tryApi++):
				
				$curl = curl_init();
				curl_setopt_array($curl, [
					CURLOPT_URL => 'https://api.close.com/api/v1/' . ltrim($data->endpoint,'/'),
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 20,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => strtoupper($data->request),
					CURLOPT_POSTFIELDS => !empty($data->fields)? json_encode($data->fields) : false,
					CURLOPT_HTTPHEADER => [
						'Content-Type: application/json',
						'Authorization: Basic ' . self::apiKey(),
					],
				]);
				$response = curl_exec($curl);
				curl_close($curl);

				if(!empty( (json_decode($response))->error )):
					// ERROR CODE FROM API // WILL TRY AGAIN
				elseif(!empty( json_decode($response) )):
					$_SESSION[$cacheKey] = $response;
					$_SESSION[$cacheTimeKey] = strtotime($useCache);
					$tryApi = 100;
					break;
				else:
					// API FAILED // WILL TRY AGAIN
					$response = json_encode((object)['error'=>'-- API ERROR -- ']);
				endif;

			endfor;

		endif;

		return json_decode($response);

	}

}
