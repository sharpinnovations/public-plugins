<?php

/* SETUP
 * https://docs.gravityforms.com/gform_after_submission/
 *****************************************************************************************************/
add_action('gform_after_submission',function( $entry, $form ){

	$closeAPI = new \SharperEdge\API\CloseIO();

	/* RESCRICT TO SPECIFIC FORMS */
	if(
		defined('SHARPEREDGE_CLOSEIO_GRAVITY_ID')
		&& !in_array($form['id'],(array)SHARPEREDGE_CLOSEIO_GRAVITY_ID)
	){
		return;
	}

	/* GET VALUES */
	$valueList = [];
	foreach( $form['fields'] as $key => $field ):
		
		$inputs = $field->get_entry_inputs();
		
		if(is_array($inputs)):
			foreach( $inputs as $k => $input ):
				$valueList[] = (object)[
					'type'	=> (string)$field->type,
					'label'	=> (string)$field->label,
					'item'	=> (string)$input['label'],
					'val'	=> rgar($entry,(string)$input['id']),
				];
			endforeach;
		else:
			$valueList[] = (object)[
				'type'	=> (string)$field->type,
				'label'	=> (string)$field->label,
				'item'	=> null,
				'val'	=> rgar($entry,(string)$field->id),
			];
		endif;
	endforeach;

	/* CHECK IF LEAD EXISTS (by email) */
	$emailAddress = null;
	foreach( $valueList as $k => $v ):
		switch(true):
			case (
				$v->type == 'email'
				|| strtolower(trim($v->label)) == 'email'
			):
				$emailAddress = trim($v->val);
				break;
		endswitch;
	endforeach;
	if(!empty($emailAddress)):
		$leadData = $closeAPI::api([
			'request' => 'get',
			'endpoint' => str_replace('"','%22',str_replace(' ','%20',
				'/lead/?query=email_address:' . $emailAddress . '&_limit=1'
			)),
		],false);
	endif;

	/* CREATE LEAD */
	$leadData = !empty($leadData->data[0])? $leadData->data[0] : null;
	if(empty($leadData->id)):

		$apiData = [
			'request' => 'post',
			'endpoint' => '/lead/',
			'fields' => [
				'name' => '',
				'contacts' => [[
					'name' => '',
				]],
			],
		];

		// DEFAULT STATUS
		if(defined('SHARPEREDGE_CLOSEIO_STATUS')){
			$apiData['fields']['status'] = SHARPEREDGE_CLOSEIO_STATUS;
		}

		// CUSTOM FIELDS
		if(defined('SHARPEREDGE_CLOSEIO_CUSTOMFIELDS')){
			$customFields = SHARPEREDGE_CLOSEIO_CUSTOMFIELDS;
			foreach( ( $customFields['.'] ?? [] ) as $key => $field):
				$apiData['fields'][$key] = $field;
			endforeach;
			foreach( ( $customFields[$form['id']] ?? [] ) as $key => $field):
				$apiData['fields'][$key] = $field;
			endforeach;
		}	

		// SET DATA
		$companyName = null;
		$contactName = null;
		$contactTitle = null;
		$contactEmail = null;
		$contactPhone = null;

		foreach( $valueList as $k => $v ):
			switch(true):

				// VALUE
				case (
					in_array(strtolower(trim($v->label)),[
						'company',
						'business',
						'organization',
					])
				):
					if(!$companyName):
						$companyName = $v->val;
						$apiData['fields']['name'] = $v->val;
					endif;
					break;

				// VALUE
				case (
					in_array(strtolower(trim($v->label)),[
						'name',
						'fullname',
						'full name',
					])
				):
					if(!$contactName):
						foreach([1,2,3,4,5] as $count ):
							if( !empty($valueList[$k+$count]) && $valueList[$k+$count]->type =='name' && $valueList[$k+$count]->val ):
								$v->val .= ' ' . $valueList[$k+$count]->val;
							endif;
						endforeach;
						$contactName = trim($v->val);
						$apiData['fields']['contacts'][0]['name'] = trim($v->val);
					endif;
					break;

				// VALUE
				case (					
					in_array(strtolower(trim($v->label)),[
						'title',
						'position',
					])
				):
					if(!$contactTitle):
						$contactTitle = trim($v->val);
						$apiData['fields']['contacts'][0]['title'] = trim($v->val);
					endif;
					break;

				// VALUE
				case (
					$v->type == 'email'
					|| in_array(strtolower(trim($v->label)),[
						'email',
						'email address',
					])
				):
					if(!$contactEmail):
						$contactEmail = trim($v->val);
						$apiData['fields']['contacts'][0]['emails'] = [[
							'type' 			=> 'office',
							'email' 		=> trim($v->val),
						]];
					endif;
					break;

				// VALUE
				case (
					$v->type == 'phone'
					|| in_array(strtolower(trim($v->label)),[
						'phone',
						'phone number',
						'phone #',
					])
				):
					if(!$contactPhone):
						$contactPhone = trim($v->val);
						$apiData['fields']['contacts'][0]['phones'] = [[
							'type' 			=> 'office',
							'phone' 		=> trim($v->val),

						]];
					endif;
					break;

			endswitch;
		endforeach;

		// SET MISSING DATA
		$apiData['fields']['name'] = !empty($apiData['fields']['name'])
			? $apiData['fields']['name']
			: ( $companyName ?: $contactName ?: $contactTitle ?: $contactEmail ?: 'New Lead'.date('m/d/Y @ h:i:s A') );

		$apiData['fields']['contacts'][0]['name'] = !empty($apiData['fields']['contacts'][0]['name'])
			? $apiData['fields']['contacts'][0]['name']
			: ( $contactName ?: $companyName ?: $contactTitle ?: $contactEmail ?: 'New Contact'.date('m/d/Y @ h:i:s A') );

		// PING API SERVICE
		$leadUpdate = $closeAPI::api($apiData,false);

	endif;

}, 10, 2 );


