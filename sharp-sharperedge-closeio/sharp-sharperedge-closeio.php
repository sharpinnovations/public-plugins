<?php
/*!
 * @wordpress-plugin
 * Plugin Name:		Sharper Edge CloseIO Integration
 * Plugin URI:		https://bitbucket.org/sharpinnovations/public-plugins/src/master/sharp-sharperedge-closeio/
 * Description:		This form syncs CLOSE IO and GRAVITY FORMS data on submission.
 * Version:			1.0.2
 * Author:			Sharp Innovations
 * Author URI:		https://www.sharpinnovations.com
 **/

// https://developer.atlassian.com/bitbucket/api/2/reference/resource/
// https://rudrastyh.com/wordpress/self-hosted-plugin-update.html

/* Settings
*****************************************************************************************************/
$plugin_info = 'https://api.bitbucket.org/2.0/repositories/sharpinnovations/public-plugins/src/master/sharp-sharperedge-closeio';

$plugin_version = '1.0.2';
$plugin_slug = 'sharp-sharperedge-closeio';

$plugin_transient = 'sharp-update'
	. '--sharp-sharperedge-closeio'
	. '--' . $plugin_slug;

/* LOAD PLUGIN CODE
*****************************************************************************************************/
require_once(__DIR__.'/update.php');

if(defined('SHARPEREDGE_CLOSEIO_APIKEY')):
	require_once(__DIR__.'/library/CloseIO.php');
	require_once(__DIR__.'/plugin.php');
endif;
